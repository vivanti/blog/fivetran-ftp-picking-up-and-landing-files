create role fivetran_vendor;
create database explore1;

grant create schema, monitor, usage
  on database explore1
  to role fivetran_vendor;

grant all
  on schema explore1.land
  to role fivetran_vendor;

grant usage
  on warehouse a_virtual_warehouse
  to role fivetran_vendor;

create user fivetran_user
  login_name        = fivetran_user
  password          = 'a-stronger-password'
  default_warehouse = a_virtual_warehouse;

grant role fivetran_vendor
  to user fivetran_vendor;
