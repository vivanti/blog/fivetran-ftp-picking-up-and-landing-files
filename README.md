# Fivetran FTP Connector

This code accompanies a blog post, to be published soon, on the
use of [Fivetran's FTP connector][1] to synchronize data from FTP
sites to places like Snowflake and Databricks.

Included is a a Docker Compose recipe for spinning up a local
vsftpd instance (Alpine edition) complete with passive mode support
and an authentication user.

To start the compose'd environment, normal tooling works:

    $ docker-compose up

[1]: https://vivanti.com/2022/08/09/fivetran-ftp-picking-up-files/
